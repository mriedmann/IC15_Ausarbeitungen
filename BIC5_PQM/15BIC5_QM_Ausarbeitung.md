---
title: "PQM Ausarbeitung"
author: "Michael Riedmann"
date: "06 November 2017"
lang: de
classoption: oneside
titlepage: true
toc: true
mainfont: "SourceSansPro-Regular"
mainfontoptions: "Scale=1.0"
subparagraph: true
---

# Einleitung

Diese Ausarbeitung besteht aus hypothetischen Fragen, die sich aus den Folien und den gegebenen Buchkapiteln ableiten. Dies sind **keine** Alt-Fragen. Da zum momentanen Zeitpunkt keine Alt-Tests zugänglich sind, ist es auch unwahrscheinlich, dass die Fragestellung realistisch ist. Trotzdem bilden sie einen guten Gesamtüberblick über den Stoff und sollten somit beim Über und Verstehen des Gebiets helfen.

*Achtung! Einige Texte und Bilder sind nicht korrekt zitiert und aus geschützten Werken entnommen. Eine Veröffentlichung dieses Dokuments ist daher nicht gestattet*

# Software Qualität

### Was ist "Qualität"?

"Qualität ist die Gesamtheit von Eigenschaften und Merkmalen eines Produktes oder einer Dienstleistung, die sich auf deren Eignung zur Erfüllung festgelegter oder vorausgesetzter Erfordernisse bezieht." - ISO 8402-1/1994

"Qualität ist die Erfüllung von (vereinbarten) Anforderungen zur dauerhaften Kundenzufriedenheit." - Zink, K.J. (1995)

### Was ist "Software Qualität"?

"Software-Qualität ist die Gesamtheit der Merkmale und Merkmalswerte eines Software-Produktes, die sich auf dessen Eignung beziehen, festgelegte Erfordernisse zu erfüllen." - DIN-ISO 9126

### In welche 2 Gruppen kann man die Qualitätsmerkmale eines Software-Produkts einteilen? Geben sie die jeweils 4 Merkmale der an!

- kundenorientiert
  - Funktionalität
  - Zuverlässigkeit
  - Effizienz
  - Benutzbarkeit

- herstellerorientiert
  - Übertragbarkeit
  - Änderbarkeit
  - Testbarkeit
  - Transparenz

### Welche Qualitätskriterien gibt es?

- External and internal quality
  - Functionality
  - Reliability
  - Usability
  - Efficiency
  - Maintainability
  - Portability
- Quality in use
  - Effectiveness
  - Productivity
  - Safety
  - Satisfaction

### Nennen Sie mindestens 4 Ursachen für Softwarefehler!

- Offensichtliche Fehler
- Schnittstellenfehler
- Fehlinterpretation von Ein-/Ausgabedaten
- Ungeprüfte Wiederverwendung von alten Codes
- Software und Hardware passen nicht zusammen
- Numerische Rundungsfehler
- Nicht ausreichende Tests
- Unterschätzen der Aufgabenstellung
- Unklare Kundenanforderungen
- Anforderungen werden nicht verstanden

### Welche Fehlerquellen gibt es?

- Lexikalische und syntaktische Fehlerquellen
- Semantische Fehlerquellen
- Parallelität als Fehlerquelle
- Numerische Fehlerquellen
- Portabilitätsfehler
- Optimierungsfehler
- Spezifikationsfehler

### Warum ist Software so schwer zu entwickeln?

- Software ist ein immaterielles Produkt.
- Software unterliegt keinem Verschleiß.
- Software altert.
- Für Software gibt es keine Ersatzteile.
- Software ist im Allgemeinen leichter und schneller änderbar als ein technisches Produkt.
- Software ist schwer zu „vermessen"
- Zunehmende Bedeutung
- Wachsende Komplexität
- Zunehmende Qualitätsanforderungen
- Nachfragestau und Engpassfaktor
- Mehr Standardsoftware
- Zunehmend „Altlasten“
- Zunehmend „Außer-Haus“-Entwicklung

# Qualitätsmanagement

### Was bedeutet "Qualitätsmanagement"?

> "Umfassende Ermittlung, exakte Festlegung und wirtschaftliche Erfüllung der Qualitätsanforderungen der Kunden"

Aufeinander abgestimmte Tätigkeiten zum Leiten und Lenken einer Organisation bezüglich Qualität.
- Qualitätspolitik
- Qualitätsziele und -planung
- Qualitätslenkung
- Qualitätsverbesserung

### Welche Entwicklungsstufen gab es bis zum modernen TQM?

|||
| --- | --- | --- |
| 1960er | Qualitätskontrolle | erfassen und korrektive Maßnahmen |
| 70/80er | Qualitätssicherung | präventive Maßnahmen |
| 90er | Qualitätsmanagement | System- und Prozessgestaltung |
| | Total Quality Management | Einbeziehung aller Systempartner |

### Wie kann ein IT-Qualitätssystem abstrakt dargestellt werden?

![](ITQMsys.png)

# Software Test

### Wie definiert man "Software Test"?
> Der Prozess, der aus allen Aktivitäten des Lebenszyklus besteht (sowohl statisch als auch dynamisch), die sich mit der Planung, Vorbereitung und Bewertung eines Softwareprodukts und dazugehöriger Arbeitsergebnisse befassen.

> "Test: (1) An activity in which a system or component is executed under speciﬁedcoditions,theresultsareobservedorrecorded,andanevaluation is made of some aspect of the system or component." - IEEE Standard Glossary of Software Engineering Terminology

### Was sind die Ziele von Software Tests?

Ziel des Prozesses ist sicherzustellen,

- dass diese allen festgelegten Anforderungen genügen,
- dass sie ihren Zweck erfüllen, und
- etwaige Fehlerzustände zu finden

#### Zwei Sichtweisen

- Systematische Verifikation von Design und Implementierung zur Überprüfung auf Übereinstimmung mit den spezifizierten Anforderungen
- Der Zweck des Testens ist es Fehler zu finden

### Was sind die 7 Grundsätze des Software Test

1. Testen zeigt die Anwesenheit von Fehlerzuständen
2. Vollständiges Testen ist nicht möglich
3. Mit dem Testen frühzeitig beginnen
4. Häufung von Fehlern
5. Wiederholungen haben keine Wirksamkeit
6. Testen ist abhängig vom Umfeld
7. „Keine Fehler bedeutet ein brauchbares System“ ist ein Trugschluss

### Wie kann der Testprozess gegliedert werden?

- Testplanung und Steuerung
- Testanalyse und Testentwurf
- Testrealisierung und Testdurchführung
- Bewertung von Endekriterien, Bericht
- Abschluss der Testaktivitäten

### Welche Testarten existieren?

Funktionaler Test
: Testen einer zu erfüllenden Funktion

Nicht-funktionaler Test
: Testen einer nicht-funktionalen Anforderung, wie Zuverlässigkeit oder Benutzbarkeit

Strukturbasierter Test
: Testen der Struktur oder Architektur der Software beziehungsweise des Systems

Fehlernachtest und Regressionstests
: Prüfen auf erfolgreiche Beseitigung eines Fehlers (Nachtest) oder Prüfen auf unbeabsichtigte beziehungsweise ungewollte Änderungen oder Seiteneffekte (Regressionstest)

### Welche Prüftechniken existieren?

#### Statischer Test – „manuelle“ Überprüfung

- Review, Walkthrough, Inspektion
:

- Werkzeuggestützt statische Analyse
: Compiler, Datenfluss, Komplexität, Kontrollfluss, statische Analyse, ...

#### Dynamischer Test – Ausführung der Software

- Black-Box Test
: Funktionaler Test, Betrachtung von Außen
- White-Box Test
: Anweisungsüberdeckung, Codeüberdeckung, Entscheidungsüberdeckung, strukturbasierter Test
- Test nicht-funktionaler Anforderungen
: Benutzbarkeitstest, Interoperabilitätstest, Lasttest, Performanztest, Portabilitätstest, Sicherheitstest, Stresstest, Wartbarkeitstest, Zuverlässigkeitstest, Usability Tests

### Welche Test Levels existieren?

- Stand-alone Test (Component Test)
: Test of a single component or of groups of components

- Integration Test
: Test to verify interfaces and how components interact via such interfaces

- System Test
: Test of the finished system against the functional and non functional requirements as i.e. performance defined in the requirements specification

- Acceptance Test
: Test cases are a subset of System Test. Should be established by customer. Usually performed on customer site

- Regression Test
: Test to avoid quality deterioration after (code) changes (patches,  function extension, change requests,... ) for each test level

# Prozessmanagement Grundlagen

### Was ist ein Prozess?

> "Ein Satz von in Wechselbeziehung oder Wechselwirkung stehenden Tätigkeiten, der Eingaben in Ergebnisse verwandelt" - ISO 9000:2008

Integrierte Gesamtheit von Tätigkeiten, mit denen ein Produkt hervorgebracht oder eine Dienstleistung bereitgestellt wird, das/die den Anforderungen der internen oder externen Kunden entspricht

### Was sind die Anspruche an einen Prozess?

- Zielgerichtet
- Messbarer In- und Output
- Fügt Wert hinzu (schafft Mehrwert)
- Wiederholbar
- Fällt in den Verantwortungsbereich einer Führungskraft

### Was sind die Komponenten/Elemente eines Prozesses?

![](ProzessKomponenten.png)

- Input
: Messbare bzw. bewertbare Informationen, Konzepte, Rohstoffe, Ressourcen, Gutachten oder Vorschriften, die zur Ausführung eines Prozessschrittes oder eines ganzen Prozesses notwendig sind.
- Output
: Messbare Ergebnisse (Dienstleistungen, Produkte) eines Prozessschrittes oder eines ganzen Prozesses mit einem (hoffentlich) messbaren Wertzuwachs für den Kunden
- Kunde
: Abnehmer einer Dienstleistung oder eines Produktes als Ergebnis eines Prozessschrittes oder eines ganzen Prozesses. Der Kundenbegriff wird auch innerhalb der Organisation verwendet (interne Kunden). Ein Prozess hat in der Regel mehrere interne oder externe Kunden.
- Lieferant
: Lieferant ist jeder, der zu einem Prozess einen Input leistet, wie z.B. Rohstoffe, Informationen, Konzepte, Vorschriften oder Gutachten, die zur Ausführung einer Tätigkeit oder eines ganzen Prozesses notwendig sind.
- Prozessschritt
: Tätigkeit oder Aufgabe innerhalb eines Prozesses, die sich im Zeitablauf ständig wiederholt und messbare Ergebnisse erzielt.

### Was ist eine Geschäftsprozess?

> Funktions- und organisationsüberschreitende Verknüpfung wertschöpfender Aktivitäten, die von Kunden erwartete Leistungen erzeugen und die aus der Strategie abgeleitete Ziele umsetzen

- Beinhaltet eine Menge von Aufgaben, die einen festgelegten Anfang und ein festgelegtes Ende haben sowie in einer festgelegten Ablauffolge ausgeführt werden
- Beeinflusst die Wettbewerbsposition eines Unternehmenslangfristig und nachhaltig
- Die Wertschöpfung eines Geschäftsprozesses besteht darin, dass Inputs mittels Einsatz von Arbeitsleistungen zu Outputs mit höherem Wertumgeformt und an externe oder interne Empfänger (Prozesskunden) ausgeliefert werden
- An einem Geschäftsprozess können mehrere Organisationseinheiten beteiligt sein

### Welche Prozesstypen gibt es?

- Kernprozesse: Arbeiten im Prozess
: Prozesse zur Herstellung von Produkten oder Dienstleistungen
- Managementprozesse: Arbeiten am Prozess
: Prozesse, wie Leitbildprozess, Prozess der strategischen Planung, Führungsprozess, Verbesserungsprozess, ...
- Supportprozesse: Arbeiten für Prozesse
: Prozesse, die unterstützend für die Kernprozesse wirken (Personalauswahl, Personalentwicklung, Buchhaltung, IT Infrastruktur, ...)

### Was ist eine Wertschöpfungskette?

> Jedes Unternehmen ist eine Ansammlung von Tätigkeiten, durch die sein Produkt entworfen, hergestellt, vertrieben, ausgeliefert und unterstützt wird. All diese Tätigkeiten lassen sich in einer Wertkette darstellen.

![](Wertschoepfungskette.png)

### Welche Ansprüche hat TQM an Prozessmanagement?

> Alle miteinander verknüpften Aktivitäten sollen verstanden und systematisch geführt werden. Entscheide sollen aufgrund von zuverlässigen Informationen und Daten gefällt werden.

- Output- / kundenorientiertes Denken
- Zielorientiertes Handeln im Sinne der Strategie
- Anwendung strukturierter Methoden um
  - Effizienz und Effektivität messbar zu machen,
  - die Basis für ständige Verbesserungen zu schaffen,
  - Wechselbeziehungen zu verdeutlichen,
  - die Entwicklung der Organisation zu fördern und voranzutreiben
- Prozessketten: Leistungserbringung als eine Abfolge von zueinander in Wechselwirkungen stehenden Prozessen
- Fehlerfreie Leistungen in fehlerfreien Prozessen
- Wiederholbarkeit eines Prozesses
- Wertsteigerung eines Prozesses

### Was sind die 5 Aspekte des Prozessmanagements?

- Steuerung
: Effizienz und Effektivität, Stellheben
- Organisation
: Wer erledigt wann und wo welche Tätigkeit, Verantwortung
- Information
: Welche Informationen werden wie weitergegeben und wo benötigt
- Kontrolle
: Erreicht der Prozess sein Ziel, Erfolgsfaktoren, Kennzahlen, Basis für Weiterentwicklung
- Sicherheit
: Wiederholbarkeit, Schnittstellen

### Was ist der Unterschied zwischen Effektivität und Effizienz?

 - Effektivität
: ein Prozess wird effektiv durchgeführt, wenn das gewünschte Ergebnis erreicht bzw. die gewünschte Wirkung erzielt wird (**"das Richtige tun"**)
 - Effizienz
: ein Prozess wird effizient durchgeführt, wenn das gewünschte Ergebnis bzw. die gesetzten Ziele wirtschaftlich (d.h. unter minimalem Ressourceneinsatz) erreicht werden (**"etwas richtig tun"**)
   - Magisches Dreieck: Kosten, Zeit, Qualität

### Wie ist der Ablauf des Prozessmanagements?

![](PMAblauf.png)

- Ausrichtung auf den Kundennutzen
- Ausrichten auf Effizienz bzw. Wirtschaftlichkeit
- Messen von Prozessgrößen (im Prozess selbst)
- Messen von Prozessergebnissen (Output von Prozessen)

### Was sind die Ziele der Prozessmodellierung?

- Schaffung von Transparenz über die betrieblichen Abläufe
- Förderung des Prozessverständnisses und der Kommunikation über Prozesse
- Eindeutige und vollständige Dokumentation der Prozesse für
  - Automatisierung von Prozessen
  - Simulation von Prozessen
  - Analyse, Messung und Verbesserung von Prozessen
  - Auditierung und Zertifizierung von Prozessen
  - Process Governance
  - Risiko- und Compliance Management in Prozessen
  - Auswahl und Implementierung von IT-Tools
  - Training und Erfahrungsaustausch

### Welche verschiedenen Sichten gibt es in der Prozessmodellierung?
- Prozesssicht
: sachlogische und zeitliche Abfolge der Prozessaktivitäten
- Ergebnis / Leistungssicht
: erstellte Prozessergebnisse
- Organisationssicht
: beteiligte Organisationseinheiten und Rollen
- Ressourcensicht
: benutzte / benötigte Ressourcen
- Daten- u. Informationssicht
: verarbeitete und erzeugte Daten, Informationen, Dateien, Dokumente
- Business Rules Sicht
: angewandte Geschäftsregeln
- Kontrollfluss
: Beziehungen zwischen den Sichten (z.B. zwischen Prozessaktivitäten, Ressourcen, Org.-Einheiten, Daten u. Ergebnissen)

# Software Engineering

### Aus welchen Punkten besteht der Lebenszyklus eines Softwaresystems? Wie stehen sie zueinander in Relation? (Skizze)

![](softwarelifecycle.png)

- Spezifikation
- Entwurf
- Implementierung
- Installation
- Betrieb

### Wie ist "Software Engineering" definiert?

> Zielorientierte Bereitstellung und systematische Verwendung von Prinzipien, Methoden, Konzepten, Notationen und Werkzeugen für die arbeitsteilige, ingenieursmäßige Entwicklung und Anwendung von umfangreichen Software-Systemen.

IEEE Computer Society Definition of Software Engineering

1. The application of a systematic, disciplined, quantifiable approach to the development, operation, and maintenance of software; that is, the application of engineering to software.
2. The study of approaches as in (1)

### Was bedeutet "Software Development Methodology"?

A Software Development Methodology or System Development Methodology in Software Engineering is a framework that is used to structure, plan, and control the process of developing an information system.

## Software Alterung

### Warum altert Software?

Im Laufe der Zeit ändern sich die Gegebenheiten der Softwareumgebung. Es sind neue Techniken, Anforderungen und Formate entstanden, die bei der Entwicklung nicht vollständig bekannt waren oder vorhergesagt werden konnten. Eine mangelnde Anpassung an diese Gegebenheiten kann zu Problemen, Fehlern oder einfach zu einem verringerten Nutzen des Programmes führen, im Extremfall bis zu dem Punkt, dass das Programm nicht mehr lauffähig ist.

Auf der anderen Seite führen gerade diese notwendigen Anpassungen, Fehlerbehebungen, Erweiterungen des Funktionsumfanges oder andere Änderungen oft zu einem unstrukturierten Programm, einem unübersichtlichen Quelltext und zu neuen Fehlern; genau dies sind die Kennzeichen der Alterung von Software.

### Welche Grundvoraussetzungen sind notwendig um Softwarealterung vorzubeugen?

#### Sensibilität
Nur wenn Entwicklung und Management davon überzeugt sind, dass interne Qualitätsparameter einen Wert besitzen ist es überhaupt möglich Ressourcen für Code-Pflege zu akquirieren.

#### Investitionsbereitschaft
Auch wenn akzeptiert wurde, dass interne Qualität wichtig ist, muss darüber hinaus auch die Bereitschaft existieren die hohen Ressourcenaufwände zu investieren. Dabei ist es essenziell die notwendigen Weichen schon zu Beginn zu stellen, da ein "Nachpflegen" oft nicht mehr möglich ist.

### Welche Gegenmaßnahmen gegen Softwarealterung gibt es?

- Refactoring
: bezeichnet die Erneuerung der inneren Strukturen eines Software-Systems, ohne sein äußeres Verhalten zu verändern.
- Redesign
: partielle oder vollständige Neuimplementierung eines Software-Systems

# Software Infrastruktur
*Wenig behandelt da nicht in den Folien*

### In welche Stadien kann die Entwicklung eines Software-Systems eingeteilt werden?

- Initialentwicklung
- Versionsdiversifizierung
- Variantendiversifizierung
- Komponentendiversifizierung

### Welche Kernaktivitäten bilden das Rückgrat der Software-Infrastruktur?

- Versionsverwaltung
- Defektmanagement
- Build- und Testautomatisierung

## SCM und ALM

### Wie wird Software Configuration Management definiert?

> "A discipline applying technical and administrative direction and surveillance to: identify and document the functional and physical characteristics of a configuration item, control changes to those characteristics, record and report change processing and implementation status, and verify compliance with specified requirements."

> "Software configuration management (SCM) is a supporting software life cycle process  which benefits project management, development and maintenance activities, assurance activities, and the customers and users of the end product."

### Wie wird Application Lifecycle Management definiert?

ALM is a set of pre-defined processes that start somewhere in the business as an idea, a need, a challenge or a risk and then pass through different development phases such as requirements definition, design, development, testing, deployment, release and maintenance spanning across an entire lifecycle of a product. Throughout the ALM process, each of these steps is closely monitored and controlled, followed by proper tracking and documentation of any changes to the application.

### Welche Komponenten sind für ALM wichtig?

- version control
- requirements management
- management of test artefacts
- issue, change and release management
- build management
- document management
- team support
- process implementation and enforcement

# Managementprozesse

## Vorgehensmodelle

### Was ist ein Vorgehensmodell?

Ein Vorgehensmodell legt fest, wie die Ausgangsgrößen eines Prozesses erarbeitet werden und bildet damit das Rückgrat des Projektmanagements. Typische Vorgehensmodelle zerlegen den Projektablauf in mehrere, konsekutiv durchlaufene Phasen. Durch die zeitliche und inhaltliche Begrenzung der verschiedenen Phasen wird die Projektdurchführung überschaubar und damit in ihrer Gesamtkomplexität verringert. Über die letzten Jahrzehnte haben sich verschiedene standardisierte Vorgehensmodelle herausgebildet, die konzeptuell erheblich voneinander abweichen.

### Warum kann man die meisten Modelle nicht in Ihrer Reinform auf gegebene Firmenorganisationen anwenden?

Nur in wenigen Fällen lassen sich Standardmodelle reibungsfrei auf eine gegebene Firmenorganisation abbilden. Ein Vorgehensmodell beschreibt stets einen idealisierten Projektverlauf und muss zunächst an die tatsächlichen Gegebenheiten angepasst werden (Individualisierung). Daher sind die weiter unten vorgestellten Vorgehensmodelle in der Praxis nur vereinzelt in ihrer Reinform anzutreffen.

## Reifegradmodelle

### Was ist ein Reifegradmodell?

Reifegradmodelle verfolgen das Ziel, die Arbeitsabläufe eines Unternehmens zu analysieren und zu optimieren. Hierzu werden die Prozesse einer Organisation anhand einer definierten Systematik auf Projekt- und Unternehmensebene bewertet und die gewonnenen Ergebnisse anschließend in Maßnahmen zur Prozessverbesserung umgesetzt.

### Was ist der Unterschied zwischen Vorgehens- und Reifegradmodell?

NebenderSchaffungproduktiverEntwicklungsprozessezielenReifegradmodelle vor allem auf die Optimierung der Organisationsstrukturen ab. Hierzu zählen unter anderem die eindeutige Verteilung von Rollen und Verantwortlichkeiten, die Einrichtung transparenter Kommunikationswege und die Deﬁnition klar geregelterEskalationsmechanismen.DamitunterscheidetsichdieZielsetzungeines Reifegradmodells grundsätzlich von der eines Vorgehensmodells. Während Vorgehensmodelle Eckpunkte und Maßnahmen für die konkrete Projektdurchführung definieren, liefern Reifegradmodelle die quantitative Basis, umsteuernd in die Arbeitsabläufe und Organisationsstrukturen einzugreifen.

# Vorgehensmodelle

## Wasserfallmodell

### Was sind die einzelnen Schritte des Wasserfallmodells?

- Systemanforderungen
- Software-Anforderungen
- Analyse
- Entwurf
- Implementierung
- Test
- Betrieb

**alternativ:**

- Anforderungsdefinition
- System und Software Design
- Implementierung und Modultest
- Integration und Systemtest
- Installation und Wartung

### Was sind Vor- und Nachteile des Wasserfallmodells?

#### Vorteile
- Dokumentation am Ende jeder Phase
- Kompatibel zu anderen Vorgehensmodellen
- Geeignet für den Einsatz bei Projekten mit klaren Anforderungen

#### Nachteile
- Anforderungen werden früh eingefroren
- Unflexibel (nachträgliche Änderungen sind nicht eingeplant)
- Die Kosten- und Ressourceneinschätzung ist ungenau

## V-Modell

### Wie heißen die einzelnen Schritte des V-Modells und wie stehen sie zueinander in Relation?

![](V-Modell.png){ width=75% }

### In welche 2 Kategorien können die Qualitätsmaßnahmen geteilt werden? Was unterschiedet sie voneinander?

- Validation
: Im Rahmen der Validation wird die Spezifikation eines Produkts im Hinblick auf den geplanten Einsatzzweck überprüft. Ob die Spezifikation im Rahmen der Software-Entwicklung fehlerfrei implementiert wurde, spielt für die Validation eine untergeordnete Rolle. Im Vordergrund steht die prinzipielle Tauglichkeit des Endprodukts und damit die Frage, ob das spezifizierte System den Anforderungen und Wünschen des Kunden entspricht.
- Verifikation
: Im Zuge der Verifikation wird überprüft, ob das entwickelte Produkt die Spezifikation erfüllt. Kurzum: Die Implementierung wird gegen die Spezifikation abgeglichen. Die Spezifikation selbst wird nicht in Frage gestellt und als korrekt vorausgesetzt. Der klassische Software-Test fällt vollständig in den Bereich der Verifikation – hier wird das gemessene Ist-Ergebnis mit dem aus der Spezifikation abgeleiteten Soll-Ergebnis abgeglichen.

### Welche Beziehung besteht zwischen dem linken und rechten Ast des V-Modells?

Insgesamt forciert das V-Modell, wie auch das Wasserfallmodell, den klassischen Top-Down-Entwurf. Die einzelnen Phasen werden im Idealfall konsekutiv durchlaufen, wobei zwischen zwei benachbarten Phasen Übergänge in beide Richtungen möglich sind. Zwischen den Entwicklungsphasen des linken Zweigs und den Verifikations- und Validationsphasen des rechten Zweigs besteht ein unmittelbarer Zusammenhang, der in Abb. 9.4 durch die waagerechten Pfeile symbolisiert wird. Es werden die Ergebnisse der verschiedenen Entwicklungsphasen unmittelbar für die Ableitung der Testfälle verwendet, die in den späteren Verifikations- und Validationsphasen zur Überprüfung der Programm- und Systemkomponenten eingesetzt werden. Die Ableitung der Testfälle erfolgt bereits während der Entwicklung, so dass zwischen den beiden Zweigen eine engere Bindung besteht, als die klassischen Visualisierungen des Modells auf den ersten Blick suggerieren.

## V-Modell XT

### Was bedeutet der Namenszusatz in V-Modell XT und wir kann dieser verstanden werden?

XT steht für eXtreme Tailoring und soll den Schwerpunkt "Anpassbarkeit" der V-Modell Weiterentwicklung darstellen. Das Modell trägt der Tatsache Rechnung, dass verschiedene Projekte in der Praxis unterschiedlich ablaufen und einer individuellen Anpassung der Arbeitsabläufe bedürfen. Diese Anpassungen passieren über vordefinierte Bausteine.

### Welche Bausteine sind im V-Modell XT definiert und wie spielen diese zusammen?

![](V-ModellBaustein.png)

Das Fundament des V-Modells XT wird durch verschiedene Vorgehensbausteinen gebildet. Für die gängigen Aufgabenstellungen eines Projekts hältdasModelleineReihevordeﬁnierterBausteinevor,diedasZusammenspielvon Produkten, Aktivitäten und Rollen festlegen. Die Produkte eines Bausteins entsprechenden Arbeitsergebnissen, die durch die festgelegten Aktivitäten erzeugt werden. Innerhalb eines Vorgehensbausteins lässt die Deﬁnition von Teilaktivitäten und untergeordneten Produkten (Themen) eine hierarchische Gliederung entstehen. Über definierte Rollen legt jeder Vorgehensbaustein die Verantwortlichkeiten für die verschiedenen Produkte und Aktivitäten fest. Die Vorgehensbausteine sind in sich abgeschlossen, interagieren jedoch miteinander über den Austausch der Arbeitsergebnisse. Die Deﬁnition entsprechender Abhängigkeiten und Querverbindungen ist expliziter Bestandteil des Modells.

### Was ist im V-Modell XT mit Produkttypus gemeint? Wie wird er bestimmt? Welche gibt es?

Um ein möglichst breites Spektrum abzudecken, wird jedes Projekt anhand gewisser charakteristischer Eigenschaften klassifiziert und einem hieraus abgeleiteten Projekttypus zugeordnet. Der Typ eines Projekts wird zum einen durch den Projektgegenstand und zum anderen durch die Projektrolle bestimmt.

### Was ist ein Projektgegenstand und welche 5 Kategorien dieser sieht das V-Modell XT vor?

Mit dem Begriff des Projektgegenstands wird im V-Modell XT das Ergebnis bezeichnet,dasimRahmeneinesProjektserarbeitetwird.IndenmeistenFällenist der Projektgegenstand ein zu erstellendes System, das einer der folgenden fünf Kategorien zugeordnet werden kann:

- Hardware-System
- Software-System
- Komplexes System
- Eingebettetes System
- Systemintegration

Alternativ kann sich der Projektgegenstand auf die Einführung und Pflege einesorganisationsspeziﬁschenVorgehensmodellsbeziehen.DieserProjektgegenstand nimmt im V-Modell XT eine besondere Stellung ein.

### Was sind Projektrollen und welche definiert das V-Modell XT?

Das V-Modell XT definiert 6 verschiedene Projektrollen, die primär das Auftraggeber- und Auftragnehmerverhältnis charakterisieren. Von außen betrachtet kann ein Projekt als Auftraggeber (AG), als Auftragnehmer (AN) oder in beiden Rollen zusammen (AG/AN) agieren. Werden zusätzlich die verschiedenen Möglichkeiten der Unterauftragsvergabe berücksichtigt, ergeben sich die folgenden 6 Projektrollen des V-Modells XT:

- Auftraggeber mit einem Auftragnehmer
- Auftraggeber mit mehreren Auftragnehmern
- Auftragnehmer ohne Unterauftragnehmer
- Auftragnehmer mit Unterauftragnehmern
- Auftraggeber/-nehmer ohne Unterauftragnehmer
- Auftraggeber/-nehmer mit Unterauftragnehmern

Jede Rolle ist im V-Modell XT mit zusätzlichen Aufgaben assoziiert, die den Besonderheiten der jeweiligen Projektkonstellation Rechnung tragen.

### Wie wird die Projektdurchführungsstrategie eines Projekts nach V-Modell XT bestimmt und was ist darunter zu verstehen?

Anhand des Projektgegenstands und der Projektrolle wird im V-Modell XT der Typ eines Projekts abgeleitet. Jeder Projekttyp wird durch eine bestimmte Projektdurchführungsstrategie beschrieben. Diese definiert eine Menge verpflichtender, sowie eine Menge optionaler Vorgehensbausteine, die zur Projektdurchführung zwingend vorgeschrieben sind oder zusätzlich in die Arbeitsabläufe integriert werden können.

### Welche Projekttypen sind im V-Modell XT definiert?

- Systementwicklungsprojekt (AG = als Auftraggeber)
- Systementwicklungsprojekt (AN = als Auftragnehmer)
- Systementwicklungsprojekt (AG/AN)
- Einführung und Pflege eines organisationsspezifischen Vorgehensmodells

## Rational Unified Process (RUP)

### Auf welche "Best Practices" ist das RUP Vorgehensmodell aufgebaut?

Das Fundament des Rational Unified Process besteht aus insgesamt 6 best practices, die allgemein anerkannte und in der Vergangenheit bewährte Arbeitsabläufe und Vorgehensweisen in sich vereinen:

- Iterative Software-Entwicklung
- Anforderungsmanagement
- Komponentenbasierte Architekturen
- Visuelle Software-Modellierung
- Software-Qualitätskontrolle
- Software-Änderungskontrolle

### In welche 4 Phasen ist das RUP Modell gegliedert?

- Konzeptionsphase (Inception Phase)
: Grobentwurf; Bestimmung der wichtigsten Geschäftsfälle und Abbildung in Use Cases. Außerdem werden Umfang, Risiko und Kosten abgeschätzt
- Entwurfsphase (Elaboration Phase)
: Spezifikationen; Festlegung der Aktivitäten der zukünftigen Phasen
- Entwicklungsphase (Construction Phase)
: Implementierung; Auch Möglichkeiten Verifikation und Validation werden erstellt
- Produktübergabe (Transition Phase)
: Übergabe an Kunden; Freigaben, Abnahmetests, Auslieferung, ...

### Was wird im RUP Modell unter einem Workflow verstanden? In welche 2 Kategorien sind diese Unterteilt?

![](rup_CoreProcesses.jpg){ width=70% }

Die vertikale Achse des Prozessmodells stellt die sogenannte Workflows, eine inhaltliche Gruppierung der Aktivitäten und Arbeitsabläufe, dar.

Die Unterteilung erfolgt in folgende 2 Kategorien:

- Core Process oder Engineering Workflows
- Supporting Workflows

### Wie heißen die 9 im RUP Modell definierten Workflows?

Unterstützende Workflows:

- Project management workflow
- Configuration and change management workflow,
- Environment workflow

Engineering Workflows:

- Business modeling workflow,
- Requirements workflow,
- Analysis and design workflow,
- Implementation workflow,
- Test workflow,
- Deployment workflow

## Agiles Modell

### Was versteht man unter "Agile Software Development"?

- Agile Software Development is a
  - group of software development methodologies
  - based on iterative and incremental development,
  - where requirements and solutions evolve
  - through collaboration between self-organizing, cross-functional teams.
- It promotes
  - adaptive planning,
  - evolutionary development and delivery,
  - a time-boxed iterative approach, and
  - encourages rapid and flexible response to change
- It is a Conceptual Framework
  - that promotes foreseen interactions throughout the development cycle.

### Was sind die Vorteile agiler Methoden gegenüber traditioneller wie dem V-Modell?

Es werden komplexe Abläufe durch wenige, flexibel anwendbare Regeln ersetzt. Im Vordergrund stehen statt Vorhersagbarkeit und Planbarkeit, die Möglichkeit auf unvorhersehbare Änderungen schnell und flexibel zu reagieren.

### Wann ist es sinnvoll agile Methoden einzusetzen?

- Geringe Kritikalität
- Erfahrene Entwickler
- Anforderungen ändern sich oft
- Kleines Entwicklerteam
- "Chaotische" Grundeinstellung des Teams

## Scrum

### Was ist "Scrum"?

Scrum (aus englisch scrum für „Gedränge“) ist ein Vorgehensmodell des Projekt- und Produktmanagements, insbesondere zur agilen Softwareentwicklung.

### Welche Rollen existieren in Scrum?

#### Product Owner
- Repräsentant des Kunden
- Verantwortung: Funktionalität, Kosten, Termine
- Product Backlog
- Release Management

#### Team
- 7 +/- 2
- Selbstorganisiert
- Gleiches Know-How Level, Hohe Qualifikation
- Verantwortung: Implementierung, Test, Umsetzung

#### Scrum Master
- Überwacht, unterstützt
- Schutz des Teams

### Was ist ein Scrum Backlog?

Ist eine dynamische, sich ständig weiterentwickelnde List an Anforderungen an das Produkt. Meist wird ein Produktbacklog als gesamte Sammelliste geführt, aus der sich dann die einzelnen Sprint Backlogs ableiten.

Die Verantwortung dafür übernimmt der Product Owner

### Was ist ein Scrum Sprint? Wie ist der Aufbau und Ablauf?

Ein Sprint ist ein Arbeitsabschnitt, in dem ein Inkrement einer Produktfunktionalität implementiert wird. Er beginnt mit einem Sprint Planning und endet mit Sprint Review und Sprint-Retrospektive. Sprints folgen unmittelbar aufeinander.

Ein Sprint umfasst ein Zeitfenster von ein bis vier Wochen. Alle Sprints sollten idealerweise die gleiche Länge haben, um so dem Projekt einen Takt zu geben. Ein Sprint wird niemals verlängert – er ist zu Ende, wenn die Zeit um ist.

![](scrum-workflow-final2.jpg)

### Was ist die Idee hinter dem Daily Meeting in Scrum?

Das "Daily Meeting" ist ein ca. 15 Minütiges Standup - Meeting bei dem Informationen innerhalb des Teams ausgetauscht werden sollen. Im Daily Scrum werden keine Probleme gelöst – vielmehr geht es darum, sich einen Überblick über den aktuellen Stand der Arbeit zu verschaffen.

- ~ 15 Minuten
- Gleicher Zeitpunkt/Ort
- Pünktlicher Start & Ende
- Standup - Meeting
- 3 Fragen
  - Was hast du seit dem letzten Daily-Scrum erledigt?
  - Gibt es Hindernisse?
  - Was willst du bis zum nächsten Daily-Scrum erledigen?
- Informationsaustausch -> keine Probleme gelöst
