---
title: "BWL Abschlusstest Ausarbeitung"
author: "Michael Riedmann"
date: "12 November 2017"
lang: de
classoption: oneside
titlepage: true
toc: true
mainfont: "SourceSansPro-Regular"
mainfontoptions: "Scale=1.0"
subparagraph: true
---

Beispiel 1: Bilanzierung (10 Punkte)
====================================

Herr Müller beginnt am 1.1.X3 seine Geschäftstätigkeit mit einem
Eigenkapital von EUR 20.000, die er seinem Unternehmen in bar zur
Verfügung stellt.

Er mietet neben einer Landstraße einen kleinen Würstelstand. Die
monatlichen Aufwendungen (Miete, Betriebskosten etc.) betragen EUR 400.

Da die alte Einrichtung nicht mehr zu gebrauchen ist, investiert er zu
Beginn des Jahres EUR 5.000 in eine neue Einrichtung mit einer
Nutzungsdauer von 5 Jahren.

**Einkauf:**
Bis zum 31.12.X3 kauft er 15.000 Paar Würstel (inkl. Gebäck) zum
Preis von je 0,60 und 16.000 Getränke zum Preis von je 0,50.

**Verkauf:**
Bis zum 31.12.X3 verkauft er 15.000 Paar Würstel (inkl. Getränke) zum
Preis von je EUR 2,00 und 15.000 Getränke zum Preis von je EUR
1,50.

Alle Transaktionen werden in bar beglichen.

Während des Jahres entnimmt Herr Müller EUR 15.000 in bar, um
seinen privaten Lebensunterhalt zu finanzieren.

<small>Hinweis: Umsatz- und Vorsteuern sind aus Vereinfachungsgründen nicht zu berücksichtigen<small>

## Antwortmöglichkeiten

* A. Die Umsatzerträge belaufen sich auf insgesamt 52.500.
* B. Der Gewinn X3 beläuft sich auf 30.200.
* C. Das Eigenkapital per 31.12.X3 beläuft sich auf 35.200.
* D. Der Handelswareneinsatz beläuft sich auf insgesamt 7.500.

## Antwort

A, B, C

------------------------ -------------------------------- ------
**Erträge**              15.000 * 2,00 + 15.000 * 1,5 =   52.500
**- HW-Einsatz**         15.000 * 0,60 + 15.000 * 0,60 =  16.500
**- sonstige Aufwände**  12 * 400 =                       4.800
**- Abschreibungen**     5000 / 5 =                       1.000
**= Gewinn**             52500 - 16500 - 4800 - 1000 =    30.200
------------------------ -------------------------------- ------

Beispiel 2: Kostenarten-, Kostenstellen- und Kostenträgerrechnung (10 Punkte)
=============================================================================

Die GuV von Herrn Friedrichs Einzelunternehmen beinhaltet im Jahr X1 die
in der nachfolgenden Tabelle angeführten Aufwendungen:

  Aufwands- bzw. Kostenarten            Aufwand
  ------------------------------------- ---------------
  Fertigungsmaterial                    700 000
  Löhne                                 1 190 000
  Gehälter                              270 000
  Abschreibungen                        230 000
  Instandhaltung                        38 000
  Zinsen                                85 000
  Werbung                               34 000
  Schadensfälle                         76 000
  Sonstige (z.B. Versicherungen etc.)   22 000
  SUMME                                 **2 645 000**

Betreffend die Überleitung der Aufwendungen in Kosten liegen folgende
Informationen vor:

-   Das Fertigungsmaterial ist in der Buchhaltung zu Anschaffungskosten
    bewertet. In der Kostenrechnung soll eine Bewertung zu
    Wiederbeschaffungskosten erfolgen; die Preise sind aufgrund der
    hohen Nachfrage um 20% gestiegen.

-   Zusätzlich zu den Gehältern der Mitarbeiter ist ein kalkulatorischer
    Unternehmerlohn in Höhe von 80.000 zu berücksichtigen.

-   Die kalkulatorischen Abschreibungen sind um 24.000 niedriger als die
    buchhalterischen Abschreibungen.

-   Im Unternehmen ist ein betriebsnotwendiges Gesamtkapital on
    2.000.000 investiert. Es wird eine Mindestverzinsung von 5%
    gefordert.

-   Bei den Schadensfällen handelt es sich um einen (versicherten) Brand
    in einer Fertigungshalle.

Die übergeleiteten Kosten sind auf die Kostenstellen Lager, Fertigung,
Verwaltung/Vertrieb und Werksküche wie folgt zu verteilen:

-   Im Lager fallen die gesamten Fertigungsmaterialkosten, 80.000 an
    Lohnkosten und 30.000 an weiteren Gemeinkosten an. Die
    Materialkosten des Lagers werden als Einzelkosten, die Lohnkosten
    des Lagers hingegen als Gemeinkosten erfasst.

-   In der Fertigung fallen im Jahr X1 bei 36.300 Arbeitsstunden einzeln
    zurechenbare Lohnkosten in Höhe von 1.090.000 und weitere
    Gemeinkosten von 120.000 an.

-   In der Werksküche fallen nicht einzeln zurechenbare Lohnkosten in
    Höhe von 20.000 und weitere Gemeinkosten von 22.000 an.

-   Die gesamten Gehälter und 228.000 an weiteren Gemeinkosten sind der
    Kostenstelle Verwaltung/Vertrieb zuzuordnen.

Die Hilfskostenstelle Werksküche ist auf Basis der Anzahl der
Mitarbeiter auf die Hauptkostenstellen umzulegen. Im Unternehmen sind
insgesamt 29 Mitarbeiter beschäftigt (davon einer in der Werksküche, zwei
im Lager, 22 in der Fertigung und vier in Verwaltung/Vertrieb).

Der Verrechnungssatz für das Lager ist auf Basis der
Materialeinzelkosten des Lagers, jener für die Fertigung auf Basis der
von ihr geleisteten Arbeitsstunden und jener für Verwaltung/Vertrieb auf
Basis der gesamten Herstellkosten zu ermitteln.

**[Aufgabenstellung:]{.underline}**

Kalkulieren Sie die Selbstkosten für folgenden Auftrag:

-   Materialkosten 4.300
-   Lohnkosten in der Fertigung 1.900
-   Arbeitsbedarf in der Fertigung 14 Stunden

## Antwortmöglichkeiten

* A. Die Gesamtkosten im Jahr X1 belaufen sich auf rd. 2.780.000
* B. Der Verrechnungssatz des Lagers beträgt rd. 13,45%
* C. Der Verrechnungssatz der Verwaltungs- und Vertriebsstelle beträgt rd. 98,59%.
* D. Die Selbstkosten des Auftrages belaufen sich auf rd. 6.200.

## Antwort

A, B

### BAB

**Kostenarten**       **Aufwand**      -        +  **Kosten**
------------------- ------------- ------ -------- ---------
Fertigungsmaterial        700.000         140.000   840.000
Löhne                   1.190.000                 1.190.000
Gehälter                  270.000          80.000   350.000
Abschreibungen            230.000 24.000            206.000
Instandhaltung             38.000                    38.000
Zinsen                     85.000 85.000  100.000   100.000
Werbung                    34.000                    34.000
Schadensfälle              76.000 76.000                  0
Sonstige                   22.000                    22.000
*SUMME*                 2.645.000 185.000 320.000 2.780.000

### Verrechnung Hilfskostenstelle

                Lager  Fertigung  Verwaltung  Küche
------------- ------- ---------- ----------- ------
Einzelkosten  840.000  1.090.000           -      -
Gemeinkosten  110.000    120.000     578.000 42.000
Mitarbeiter         2         22           4      1
GK(Küche)       3.000     33.000       6.000      -

### Verrechnung Hauptkostenstellen

K. Stelle  Formel                                                 Satz
---------- ------------------------------------------------------ -------
Küche      MA * ( 42.000 / (29 - 1))
Lager      (110.000 + 3000) / 840.000                             13,45%
Fertigung  (120.000 + 33.000) / 36.300                            4,215
Verwaltung (578k + 6k) / (840k + 110k + 3k + 1.090k + 120k + 33k) 26,59%


### Angebot
-------------- ----------------------------- -----
GK(Lager)      4300 * 0,1345 =               578,6
GK(Fertigung)  14 * 4,215 =                  59,01
Herstellkosten 4300 + 1900 + 578,6 + 59,01 = 6837
GK(Verwaltung) 6837 * 0,2659 =               1818
Selbstkosten   6837 + 1818 =                 8655
-------------- ----------------------------- -----

Beispiel 3: Periodenerfolgsrechnung (10 Punkte)
===============================================

Das Unternehmen Felgen KG produziert Autofelgen in unterschiedlichen
Ausführungen für die Autoindustrie. Aus der vergangenen
Abrechnungsperiode liegen folgende Angaben vor:

  Felgentyp                         Spielberg   Budapest   Salzburg
  --------------------------------- ----------- ---------- ----------
  Produktionsmenge (Stück)          18.000      21.000     10.000
  Absatzmenge (Stück)               18.000      20.000     10.000
  Preis pro Stück                   102,00      90,00      128,00
  Fertigungsmaterial pro Stück      48,00       40,00      60,00
  Min. pro Stück in F1              6,00        6,00       10,00
  kg pro Stück in F2                1,00        2,50       2,50
  Fertigungslöhne pro Stück in F3   2,20        1,20       2,40

Folgende Verrechnungssätze sind aus der Kostenstellenrechnung bekannt:

-   Fertigungsstelle 1: 48 pro Stunde

-   Fertigungsstelle 2: 4 pro kg

-   Fertigungsstelle 3: 200% der Fertigungslöhne in F3

-   Verwaltungs- und Vertriebsstelle: 25% der Herstellkosten der
    abgesetzten Menge

    **[Aufgabenstellung:]{.underline}**

    Ermitteln Sie den Periodenerfolg nach dem **Umsatzkostenverfahren**!

## Antwortmöglichkeiten

* A. Die Herstellkosten pro Stück des Produkts Spielberg
betragen rd. 63,40.
* B. Die Herstellkosten der in der Periode abgesetzten Produkte
betragen insgesamt rd. 3.161.200.
* C. Der Periodenerfolg beläuft sich insgesamt auf rd. 964.500.
* D. Hätte man den Periodenerfolg nach dem
Gesamtkostenverfahren ermittelt würde man zum gleichen
Ergebnis gelangen.

## Antwort

- D^1^
- A, B, C, D^2^

### Spielberg

-------- ----------------------------------- -----------
Material                                      48,0 €/Stk
F1       48 / 60 * 6                           4,8 €/Stk
F2       4 * 1                                 4,0 €/Stk
F3^1^    2,2 * 200%                            4,4 €/Stk
F3^2^    2,2 + 2,2 * 200%                      6,6 €/Stk
Summe^1^                                      61,2 €/Stk
Summe^2^                                      63,4 €/Stk
-------- ----------------------------------- ------------

- Erlös: 18.000 * 102 = 1.836.000
- Kosten^1^: 18.000 * 61,2 = 1.101.600
- Kosten^2^: 18.000 * 63,4 = 1.141.200

### Budapest

-------- ----------------------------------- -----------
Material                                      40,0 €/Stk
F1       48 / 60 * 6                           4,8 €/Stk
F2       4 * 2,5                              10,0 €/Stk
F3^1^    1,2 * 200%                            2,4 €/Stk
F3^2^    1,2 + 1,2 * 200%                      3,6 €/Stk
Summe^1^                                      57,2 €/Stk
Summe^2^                                      58,4 €/Stk
-------- ----------------------------------- ------------

- Erlös: 20.000 * 90 = 1.800.000
- Kosten^1^: 20.000 * 57,2 = 1.144.000
- Kosten^2^: 20.000 * 58,4 = 1.168.000

### Salzburg

-------- ----------------------------------- -----------
Material                                      60,0 €/Stk
F1       48 / 60 * 10                          8,0 €/Stk
F2       4 * 2,5                              10,0 €/Stk
F3^1^    2,4 * 200%                            4,8 €/Stk
F3^2^    2,4 + 2,4 * 200%                      7,2 €/Stk
Summe^1^                                      82,8 €/Stk
Summe^2^                                      85,2 €/Stk
-------- ----------------------------------- ------------

- Erlös: 10.000 * 128 = 1.280.000
- Kosten^1^: 10.000 * 82,8 = 828.000
- Kosten^2^: 10.000 * 85,2 = 852.000

### Berechnung^1^

**Felgentyp**            **Spielberg**   **Budapest**   **Salzburg**   **Summe**
------------------------ --------------- -------------- -------------- -----------
\ Erlöse                       1.836.000      1.800.000      1.280.000 4.916.000
\- HK abgesetzte Menge         1.101.600      1.144.000        828.000 3.073.600
\- VwVt-Gemeinkosten             275.400        286.000        207.000 768.400
= Periodenerfolg                 459.000        370.000        245.000 1.074.000

### Berechnung^2^

**Felgentyp**            **Spielberg**   **Budapest**   **Salzburg**   **Summe**
------------------------ --------------- -------------- -------------- -----------
\ Erlöse                       1.836.000      1.800.000      1.280.000 4.916.000
\- HK abgesetzte Menge         1.141.200      1.168.000        852.000 3.161.200
\- VwVt-Gemeinkosten             285.300        292.000        213.000 790.300
= Periodenerfolg                 409.500        340.000        215.000 964.500

Beispiel 4: Programmplanung (10 Punkte)
=======================================

In einem Gewerbebetrieb können auf einer Fertigungsanlage 4 Produkte
(P1, P2, P3 und P4) erzeugt werden. Die Kapazität dieser Anlage beträgt
in der kommenden Planungsperiode 2.400 Maschinenstunden (Mh).

Hinsichtlich der Erlöse und Kosten, der Kapazitätsinanspruchnahme sowie
der Absatzobergrenzen liegen über die vier Produkte folgende Daten vor:

  Produkt                      P1    P2    P3    P4
  ---------------------------- ----- ----- ----- -----
  Nettoerlös / Stk.            240   300   420   260
  Variable Kosten / Stk.       160   260   300   240
  Mh / Stk.                    4     0,5   3     2
  Absatzobergrenze (in Stk.)   480   480   340   400

Die Fixkosten je Periode betragen 40.000.

## Antwortmöglichkeiten

* A.
Um die maximal absetzbaren Mengen aller Produkte zu
produzieren wären 2.980 Mh nötig.
* B.
Der relative Deckungsbeitrag von Produkt 4 beträgt 10. Das ist zu
wenig, um in das optimale Progamm aufgenommen zu werden.
* C.
Im optimalen Programm werden von Produkt 3 480 Stück
produziert.
* D.
Der mit dem optimalen Programm maximal erzielbare
Periodenerfolg beträgt 82.800

## Antwort

B

              P1      P2      P3      P4   Summe
-------- ------- ------- ------- ------- -------
DB            80      40     120      20
Mh/Stk         4     0,5       3       2
rel. DB       20      80      40      10
Priorität      3       1       2       4
Absatz       480     480     340     400
Mh max     1.920     240   1.020     800   3.980
Mh prog    1.140     240   1.020       0   2.400
Stk          285     480     340       0
DB prog   22.800  19.200  40.800       0  82.800

Periodenerfolg: 82.800 - 40.000 = 42.000

Beispiel 5: Kapitalwert (10)
============================

Ein Unternehmen plant die Beschaffung einer neuen Produktionsanlage. Die
Anschaffungsauszahlung beträgt 60.000, die betriebliche Nutzungsdauer
vier Jahre. In diesen vier Jahren können durch das erzeugte Produkt
Umsatzerlöse von 15.000, 20.000, 25.000 bzw. 30.000 erzielt werden. Mit
einer Wahrscheinlichkeit von 20% fallen die Umsatzerlöse sogar um 30%
höher aus.

Das Unternehmen beliefert nur Stammkunden denen es großzügige
Zahlungskonditionen gewährt. So muss im Jahr des Absatzes nur eine
Anzahlung von 60% geleistet und der Restbetrag erst ein Jahr später
bezahlt werden.

Nach Ablauf der betrieblichen Nutzungsdauer muss die Produktionsanlage
zerlegt, gesäubert und entsorgt werden. Dafür fallen am Ende des
vierten, fünften und sechsten Jahres Auszahlungen von jeweils 7.000 an.

Der risikolose Zinssatz beträgt 6%. Dem Risiko der Investition wird mit
einem Risikozuschlag von 3 Prozentpunkten Rechnung getragen.

**[Aufgabenstellung:]{.underline}**

Ermitteln Sie mittels Risikozuschlagsmethode den **Kapitalwert** der
Investition und beurteilen Sie deren Vorteilhaftigkeit!

## Antwortmöglichkeiten

* A.
Der Kapitalwert der Investition ist positiv. Die Investition sollte daher
durchgeführt werden.
* B.
Der Einzahlungsüberschuss des 4. Jahres beläuft sich auf 22.680.
* C.
Der Kapitalwert beträgt rd. -751,80
* D.
Der Kapitalwert beträgt rd. +751,80.

## Antwort

B, C

- Investment = 60.000
- Erwartungswert = ( x * 80% ) + ((x * 130%) * 20%)
- Einzahlungsüberschuss = Erwartungswert~(n-1)~ * 40% + Erwartungswert~n~ * 60% - Ausgaben
- Einzahlungsüberschuss abgezinst = Erlös / (1,09)^n^

                       1       2       3       4       5       6
---------------- ------- ------- ------- ------- ------- -------
Umsatzerlöse      15.000  20.000  25.000  30.000
Erwartungswert    15.900  21.200  26.500  31.800
Ausgaben               0       0       0   7.000  7.000   7.000
Einzahlungsü.      9.540  19.080  24.380  22.680  5.720  -7.000
abgezinst          8.752  16.059  18.826  16.067  3.718  -4.174

Kapitalwert = Summe Erlöse - Investition = 59.248,20
- 60.000 = -751,80

Beispiel 6: Kurzfristiger Finanzplan (5 Punkte)
===============================================

Für Zwecke der kurzfristigen Finanzplanung (Jänner bis Mai 2017) liegen
in einem Unternehmen folgende Plan-Informationen vor:

Das Kontokorrentkonto weist per Anfang Jänner einen negativen Saldo in
Höhe von 4.370 auf.

Der zahlungswirksame Deckungsbeitrag beträgt laut Planung im Jänner
78.000; er steigt in den Folgemonaten um monatlich 2%.

Die zahlungswirksamen Fixkosten betragen 8.400 im Jänner; sie steigen in
den Folgemonaten um ebenfalls um monatlich 2%.

Die zahlungswirksamen Privatentnahmen betragen 28.000 im Jänner; sie
steigen in den Folgemonaten um monatlich 3%.

Für Investitionen werden folgende Auszahlungen geplant: 90.000 im
Jänner, 63.000 im Februar und 17.600 im April.

Im Jänner wird ein Bankkredit in Höhe von 60.000 aufgenommen. Der Kredit
wird mit 4.% p.m. verzinst und ist beginnend im Jänner in sechs gleichen
Annuitäten zu tilgen.

Die Kontokorrentzinsen betragen 0,1% p.m. im Haben und 2,0% p.m. im
Soll. Aus Vereinfachungsgründen wird in der Planung nur der monatliche
Anfangsbestand am Kontokorrentkonto verzinst.

Eine Überziehung des Kontokorrentkontos um bis zu 50.000 ist ohne Überziehungsgebühr möglich. Weitere Gebühren können in der Planung
vereinfachend vernachlässigt werden.

**[Aufgabenstellung:]{.underline}**

Vervollständigen Sie den nachfolgenden **kurzfristigen Finanzplan**!
Monatliche Finanzmitteldefizite und -überschüsse (inkl. Kontokorrentzinsen) werden über das Kontokorrentkonto ausgeglichen.

## Antwortmöglichkeiten

* A. Die Einzahlungen im Jänner belaufen sich auf 138.000.
* B. Die Annuität des Kredits beläuft sich auf rd. 11.445,71.
* C. Im April beträgt die Belastung mit Kontokorrentzinsen -123,11.
* D. Der Kontokorrentstand Ende April/Anfang Mai beträgt rd. -6.155,62.

## Antwort

A, B, C

  **Monat**                      **Januar**     **Februar**       **März**      **April**        **Mai**
  ------------------------- --------------- --------------- -------------- -------------- --------------
  **AB Kontokorrentkonto**    **-4 370,00**   **-4 303,11** **-36 682,88**  **-6 155,61**   **7 939,29**
  Deckungsbeitrag               78 000,00       79 560,00      81 151,20      82 774,22      84 429,71
  Kreditaufnahme                60 000,00
  **Summe Einzahlungen**     **138 000,00**   **79 560,00**  **81 151,20**  **82 774,22**  **84 429,71**
  Fixkosten                     -8 400,00       -8 568,00      -8 739,36      -8 914,15      -9 092,43
  Investitionen                -90 000,00      -63 000,00           0,00     -17 600,00           0,00
  Kredittilgungen              -11 445,71      -11 445,71     -11 445,71     -11 445,71     -11 445,71
  Privatentnahmen              -28 000,00      -28 840,00     -29 705,20     -30 596,36     -31 514,25
  **Summe Auszahlungen**    **-137 845,71** **-111 853,71** **-49 890,27** **-68 556,21** **-52 052,39**
  **Kontokorrentzinsen**         **-87,40**      **-86,06**    **-733,66**    **-123,11**       **7,94**

  Beispiel 7: Unternehmensbesteuerung (5 Punkte)
==============================================

Welche der nachfolgenden **Aussagen** sind richtig? Kreuzen sie die
richtigen Aussagen an!

A. Die Kapitalertragsteuer hat in bestimmten Fällen Endbesteuerungswirkung. Dies bedeutet, dass die Einkommensteuer durch den Kapitalertragsteuerabzug abgegolten ist und die Kapitalerträge (z.B. Dividenden) weder beim Gesamtbetrag der Einkünfte noch beim Einkommen zu berücksichtigen sind.

B. Die effektive Belastung von an natürliche Personen ausgeschütteten Gewinnen einer Kapitalgesellschaft beträgt 52,5%. Diese Belastung ergibt sich aus der 25%igen Körperschaftsteuer zuzüglich der 27,5%igen Kapitalertragsteuer für Dividenden.

C. Werden Gewinnanteil einer inländischen Kapitalgesellschaft an eine andere inländische Kapitalgesellschaft ausgeschüttet, so sind diese bei der empfangenden Gesellschaft unabhängig von der Beteiligungshöhe steuerfrei.

D. Umsatzsteuerbefreiungen mit Vorsteuerabzug werden als unechte Steuerbefreiungen bezeichnet.

## Antwort

A, C
