$BuildArgs = @{
     Inputs = {
        Get-ChildItem . -Recurse -Filter *.md
     }
     Outputs = {process{
         $file = Get-Item $_
         $dir = [System.IO.Path]::Combine($PSScriptRoot, "build")
         $name = "$($file.BaseName).pdf"
         $path = [System.IO.Path]::Combine($dir, $name)
         $path | Out-Host
         $path
     }}
 }

# Synopsis: Build the project.
task Build @BuildArgs -Partial {
    begin { if(-not (Test-Path .\build)){ New-Item -Type Directory .\build } }
    process{
      exec {
        $wd = (Get-Item $_).Directory
        $tmplpath = [System.IO.Path]::Combine($wd,"template.tex")
        $respath = [System.IO.Path]::Combine($wd,"assets")
        pandoc $_ "--template=$tmplpath" "--resource-path=$respath" --standalone --pdf-engine=xelatex --from=markdown --listings "--output=$2"
      }
    }
  }

# Synopsis: Remove temp files.
task Clean {
    Remove-Item build\* -Recurse -Force -ErrorAction 0
}

# Synopsis: Build and clean.
task . Build
